PKGS=sdl2
CFLAGS=-Wall -Wold-style-definition -O3 -ggdb -std=c11 -pedantic `pkg-config --cflags ${PKGS}`
LIBS=`pkg-config --libs ${PKGS}` -lm
COMMON_FILES=src/game.h src/game.c src/render.h src/render.c

gp: ${COMMON_FILES}
	${CC} ${CFLAGS} src/gp.c -o gp ${LIBS}

run: gp
	./gp

clean:
	rm gp
