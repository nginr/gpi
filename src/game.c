#include "game.h"

Coord coord_dirs[4] = {
    // DIR_RIGHT,
    {1, 0},
    // DIR_UP,
    {0, -1},
    // DIR_LEFT,
    {-1, 0},
    // DIR_DOWN,
    {0, 1},
};

int coord_equals(Coord a, Coord b) {
    return a.x == b.x && a.y == b.y;
}

int mod(int a, int b){ return (a % b + b) % b; }

int random_int_range(int lo, int hi){
    return rand()% (hi - lo)+lo;
}
int is_cell_empty(const Game* game, Coord coord){
    for (size_t i = 0; i < AGENTS_CNT; ++i){
        if (coord_equals(game->agents[i].pos, coord)){
            return 0;
        }
    }
    for (size_t i = 0; i < FOOD_CNT; ++i){
        if (coord_equals(game->foods[i].pos, coord)) return 0;
    }
    for (size_t i = 0; i < WALL_CNT; ++i){
        if (coord_equals(game->walls[i].pos, coord)) return 0;
    }
    return 1;
}

void print_chromos(FILE* stream, const Chromosome* chromo) {
    for (size_t i = 0; i < JEANS_CNT; ++i) {
        Gene g = chromo->jeans[i];
        fprintf(stream, "%d %s %s %d\n",
                g.state,
                env_as_cstr(g.env),
                action_as_cstr(g.action),
                g.next_state
        );
    }
}

void step_agent(Agent* agent) {
    Coord d = coord_dirs[agent->dir];
    agent->pos.x = mod(agent->pos.x + d.x, BOARD_WIDTH);
    agent->pos.y = mod(agent->pos.y + d.y, BOARD_HEIGHT);
}

void perform_action(Game* game, size_t a_indx, Action actn) {
    switch (actn) {
        case ACTION_NOP:
            break;
        case ACTION_STEP:
            if (env_of_agent(game, a_indx) != ENV_WALL) {
                step_agent(&game->agents[a_indx]);
            }
            break;
        case ACTION_EAT: {
            Food* food = food_infront(game, a_indx);
            if (food != NULL) {
                /* step_agent(&game->agents[indx]); */
                food->eaten = 1;
                game->agents[a_indx].hunger += FOOD_POINTS;
                if (game->agents[a_indx].hunger > HUNGER_MAX)
                    game->agents[a_indx].hunger = HUNGER_MAX;
            }
            } break;
        case ACTION_ATTACK: {
            Agent* enemy = agent_infront(game, a_indx);
            if (enemy != NULL)
                enemy->health -= ATTACK_DAMAGE;
            } break;
        case ACTION_ROTATE_LEFT:
            game->agents[a_indx].dir = mod(game->agents[a_indx].dir + 1, 4);
            break;
        case ACTION_ROTATE_RIGHT:
            game->agents[a_indx].dir = mod(game->agents[a_indx].dir - 1, 4);
            break;
        case ACTION_CNT:
            assert(0 && "UNREACHABLE.");
            break;
    }
}

void step_game(Game* game) {
    // Interpret Genes
    for (size_t i = 0; i < AGENTS_CNT; ++i) {
        if (game->agents[i].health > 0) {
            for (size_t j = 0; j < JEANS_CNT; ++j) {
                Gene gene =game->chromos[i].jeans[j];
                if (gene.state == game->agents[i].state &&
                    gene.env == env_of_agent(game, i) )
                {
                   perform_action(game, i, gene.action);
                   game->agents[i].state = gene.next_state;
                   break;
                }
            }
            // Apply hunger
            game->agents[i].hunger -= STEP_HUNGER_DAMAGE;
            if (game->agents[i].hunger <= 0) game->agents[i].health = 0;
            game->agents[i].lifetime += 1;
        }
    }
}

void init_game(Game* game) {
    for (size_t i = 0; i < AGENTS_CNT; ++i) {
        game->agents[i].pos = random_empty_coord_on_board(game);
        game->agents[i].dir = random_dir();
        game->agents[i].hunger = HUNGER_MAX;
        game->agents[i].health = HEALTH_MAX;
        game->agents[i].dir = i%4;
        game->agents[i].lifetime = 0;

        for (size_t j = 0; j < JEANS_CNT; ++j){
            game->chromos[i].jeans[j].state = random_int_range(0, STATE_CNT);
            game->chromos[i].jeans[j].env =    random_env();
            game->chromos[i].jeans[j].action = random_action();
            game->chromos[i].jeans[j].next_state = random_int_range(0, STATE_CNT);
        }
    }

    for (size_t i = 0; i < FOOD_CNT; ++i) {
        game->foods[i].pos = random_empty_coord_on_board(game);
    }

    for (size_t i = 0; i < WALL_CNT; ++i) {
        game->walls[i].pos = random_empty_coord_on_board(game);
    }
}

const char* dir_as_cstr(Dir dir) {
    switch (dir) {
        case  DIR_RIGHT:             return "DIR_RIGHT";
        case  DIR_UP   :             return "DIR_UP";
        case  DIR_LEFT :             return "DIR_LEFT";
        case  DIR_DOWN :             return "DIR_DOWN";
        default :{}
    }
    //return "UNREACHABLE";
    assert(0 && "UNREACHABLE");
}

const char* env_as_cstr(Env env) {
    switch (env) {
        case ENV_NOP  :             return "ENV_NOP";
        case ENV_AGENT:             return "ENV_AGENT";
        case ENV_FOOD :             return "ENV_FOOD";
        case ENV_WALL :             return "ENV_WALL";
        default :{}
    }
    //return "UNREACHABLE";
    assert(0 && "UNREACHABLE");
}

const char* action_as_cstr(Action actn) {
    switch (actn) {
        case ACTION_NOP:            return "ACTION_NOP";
        case ACTION_STEP:           return "ACTION_STEP";
        case ACTION_EAT:            return "ACTION_EAT";
        case ACTION_ATTACK:         return "ACTION_ATTACK";
        case ACTION_ROTATE_LEFT:    return "ACTION_ROTATE_LEFT";
        case ACTION_ROTATE_RIGHT:   return "ACTION_ROTATE_RIGHT";
        default :{}
    }
    //return "UNREACHABLE";
    assert(0 && "UNREACHABLE");
}

Dir random_dir(void) {
    return (Dir)random_int_range(0, 4);
}

Env env_of_agent(Game* game, size_t agent_indx) {

    if  (food_infront(game, agent_indx) != NULL) return ENV_FOOD;
    if  (wall_infront(game, agent_indx) != NULL) return ENV_WALL;
    if (agent_infront(game, agent_indx) != NULL) return ENV_AGENT;
    return ENV_NOP;
}
Env random_env(void){
    return random_int_range(0, ENV_CNT);
}

Wall* wall_infront(Game* game, size_t a_indx) {
    Coord infront = coord_infront(&game->agents[a_indx]);
    for (size_t i = 0; i < WALL_CNT; ++i){
        if ( coord_equals( infront, game->walls[i].pos ) ){
            return &game->walls[i];
        }
    }
    return NULL;
}

Food* food_infront(Game* game, size_t a_indx) {
    Coord infront = coord_infront(&game->agents[a_indx]);
    for (size_t i = 0; i < FOOD_CNT; ++i){
        if ( !game->foods[i].eaten && coord_equals( infront, game->foods[i].pos ) ){
            return &game->foods[i];
        }
    }
    return NULL;
}

Coord random_coord_on_board(void) {
    Coord result;
    result.x = random_int_range(0, BOARD_WIDTH);
    result.y = random_int_range(0, BOARD_WIDTH);
    return result;
}
Coord random_empty_coord_on_board(const Game* game) {
    Coord result = random_coord_on_board();
    while(!is_cell_empty(game, result)) {
        result = random_coord_on_board();
    }
    return result;
}

Coord coord_infront(const Agent* agent) {
    Coord d = coord_dirs[agent->dir];
    Coord result = agent->pos;
    // The board wraps around.....
    result.x = mod(result.x + d.x, BOARD_WIDTH);
    result.y = mod(result.y + d.y, BOARD_HEIGHT);
    return result;
}

void  print_agent(FILE* stream, Agent* agent) {
    fprintf(stream, "Agent {\n");
    fprintf(stream, "  .pos = ( %d, %d )\n", agent->pos.x, agent->pos.y);
    fprintf(stream, "  .dir = %s\n", dir_as_cstr(agent->dir));
    fprintf(stream, "  .hunger = %d\n", agent->hunger);
    fprintf(stream, "  .health = %d\n", agent->health);
    fprintf(stream, "  .lifetime = %d\n", agent->lifetime);
    fprintf(stream, "  .state = %d\n", agent->state);
    fprintf(stream, "}\n");
}

Agent* agent_infront(Game* game, size_t a_indx) {
    Coord infront = coord_infront(&game->agents[a_indx]);
    for (size_t i = 0; i < WALL_CNT; ++i){
        // i cannot be a_indx unless the board is 1x1
        // because the board wraps around
        if ( i!=a_indx && game->agents[i].health > 0 && coord_equals( infront, game->agents[i].pos ) ){
            return &game->agents[i];
        }
    }
    return NULL;
}

Agent* agent_at(Game* game, Coord pos) {
    for (size_t i = 0; i < AGENTS_CNT; ++i) {
        if (coord_equals(game->agents[i].pos, pos)) {
            return &game->agents[i];
        }
    }
    return NULL;
}

Action random_action(void){
    return random_int_range(0, ACTION_CNT);
}
