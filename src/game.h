#ifndef GAME_H_
#define GAME_H_

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#define BOARD_WIDTH 10
#define BOARD_HEIGHT 10

#define AGENTS_CNT 15
#define FOOD_CNT 15
#define WALL_CNT 15
#define JEANS_CNT 20

#define STATE_CNT 5

#define FOOD_POINTS 10
#define STEP_HUNGER_DAMAGE 5
#define HUNGER_MAX 100
#define HEALTH_MAX 100
#define ATTACK_DAMAGE 10

static_assert(AGENTS_CNT + FOOD_CNT + WALL_CNT <= BOARD_WIDTH * BOARD_HEIGHT, "Entities can not exceed the environment space.");

typedef struct {
    int x, y;
} Coord;

typedef int State;

typedef enum {
    ENV_NOP = 0,
    ENV_AGENT,
    ENV_FOOD,
    ENV_WALL,
    ENV_CNT,
} Env;

typedef enum {
    DIR_RIGHT = 0,
    DIR_UP,
    DIR_LEFT,
    DIR_DOWN,
    DIR_CNT,
} Dir;

typedef enum {
    ACTION_NOP = 0,
    ACTION_STEP,
    ACTION_EAT,
    ACTION_ATTACK,
    ACTION_ROTATE_LEFT,
    ACTION_ROTATE_RIGHT,
    ACTION_CNT,
} Action;

typedef struct {
    State state;
    Env env;
    Action action;
    State next_state;
} Gene;

typedef struct {
    size_t cnt;
    Gene jeans[JEANS_CNT];
} Chromosome;

typedef struct {
    Coord pos;
    Dir dir;
    int hunger;
    int health;
    int lifetime;
    State state;
} Agent;

typedef struct {
    int eaten;
    Coord pos;
} Food;

typedef struct {
    Coord pos;
} Wall;

typedef struct {
    Agent agents[AGENTS_CNT];
    Chromosome chromos[AGENTS_CNT];
    Food foods[FOOD_CNT];
    Wall walls[WALL_CNT];
} Game;

int    coord_equals(Coord a, Coord b);
int    mod(int a, int b);
int    random_int_range(int lo, int hi);
int    is_cell_empty(const Game* game, Coord coord);
void   print_chromos(FILE* stream, const Chromosome* chromo);
void   step_agent(Agent* agent);
void   perform_action(Game* game, size_t a_indx, Action actn);
void   step_game(Game* game);
void   init_game(Game* game);
void  print_agent(FILE* stream, Agent* agent);
const  char* dir_as_cstr(Dir dir);
const  char* env_as_cstr(Env env);
const  char* action_as_cstr(Action actn);
Dir    random_dir(void);
Env    env_of_agent(Game* game, size_t agent_indx);
Env    random_env(void);
Wall*  wall_infront(Game* game, size_t a_indx);
Food*  food_infront(Game* game, size_t a_indx);
Coord  random_coord_on_board(void);
Coord  random_empty_coord_on_board(const Game* game);
Coord  coord_infront(const Agent* agent);
Agent* agent_infront(Game* game, size_t a_indx);
Agent* agent_at(Game* game, Coord pos);
Action random_action(void);


#endif // GAME_H_
