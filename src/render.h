#ifndef RENDER_H_
#define RENDER_H_

#include <SDL2/SDL.h>
#include "game.h"

#define BG_C          0x181818FF
#define GRID_C        0x748CABFF
#define AGENT_ALIVE_C 0xDA2C38FF
#define AGENT_DEAD_C  0x909090FF
#define WALL_C        0x748CABFF
#define FOOD_C        0x87C38FFF

#define SCREEN_WIDTH  800
#define SCREEN_HEIGHT 800

#define CELL_WIDTH ((float)SCREEN_WIDTH / (float)BOARD_WIDTH)
#define CELL_HEIGHT  ((float)SCREEN_HEIGHT / (float)BOARD_HEIGHT)
#define A_PADDING (fminf(CELL_WIDTH, CELL_HEIGHT) / 5.0f)
#define F_PADDING (A_PADDING)
#define W_PADDING 20


#define HEX_C(hex) \
        ((hex) >> (8 * 3)) & 0xFF,\
        ((hex) >> (8 * 2)) & 0xFF,\
        ((hex) >> (8 * 1)) & 0xFF,\
        ((hex) >> (8 * 0)) & 0xFF


typedef struct {   int cx, cy, r;   }
                        Circle;

typedef struct {    int x1, y1;    int x2, y2;    int x3, y3;    }
                                    Triangle;


int  sdlCheckCode(int code);
void *sdlCheckPointer(void* ptr);

void render_board(SDL_Renderer* renderer);
void render_agent(SDL_Renderer* renderer, Agent agent);
void render_food(SDL_Renderer* renderer, Food food);
void render_wall(SDL_Renderer* renderer, Wall wall);
void render_game(SDL_Renderer* renderer, const Game* game);

void hollow_circle(SDL_Renderer * renderer, Circle circle);
void fill_circle(SDL_Renderer * renderer, Circle circle);

void swap_points(int* x, int* y);
void fill_bottom_flat_triangle(SDL_Renderer* renderer, Triangle t);
void fill_top_flat_triangle(SDL_Renderer* renderer, Triangle t);
void fill_triangle(SDL_Renderer* renderer, Triangle tri);

Triangle triangle_sorted_by_y(Triangle t);

#endif // RENDER_H_
