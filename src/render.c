#include "render.h"


void hollow_circle(SDL_Renderer * renderer, Circle circle) {

    int dx = 0, dy = circle.r, d = circle.r - 1;

    // Iterate only one-eithth of a circle
    while (dy >= dx) {
        SDL_RenderDrawPoint(renderer, circle.cx + dx, circle.cy + dy);
        SDL_RenderDrawPoint(renderer, circle.cx + dy, circle.cy + dx);
        SDL_RenderDrawPoint(renderer, circle.cx - dx, circle.cy + dy);
        SDL_RenderDrawPoint(renderer, circle.cx - dy, circle.cy + dx);
        SDL_RenderDrawPoint(renderer, circle.cx + dx, circle.cy - dy);
        SDL_RenderDrawPoint(renderer, circle.cx + dy, circle.cy - dx);
        SDL_RenderDrawPoint(renderer, circle.cx - dx, circle.cy - dy);
        SDL_RenderDrawPoint(renderer, circle.cx - dy, circle.cy - dx);


        if (d >= 2 * dx) { d -= 2 * dx + 1; dx++; }
        else if (d < 2 * (circle.r - dy)) { d += 2 * dy - 1; dy--; }
        else { d += 2 * (dy - dx - 1); dy--; dx++; }
    }
}


void fill_circle(SDL_Renderer * renderer, Circle circle) {

    int dx = 0, dy = circle.r, d = circle.r - 1;

    // Iterate only one-eithth of a circle
    while (dy >= dx) {
        // Draw a line from one point through the diameter of the circle to the oppposite
        // point
        SDL_RenderDrawLine(renderer, circle.cx - dy, circle.cy + dx, circle.cx + dy, circle.cy + dx);
        SDL_RenderDrawLine(renderer, circle.cx - dx, circle.cy + dy, circle.cx + dx, circle.cy + dy);
        SDL_RenderDrawLine(renderer, circle.cx - dx, circle.cy - dy, circle.cx + dx, circle.cy - dy);
        SDL_RenderDrawLine(renderer, circle.cx - dy, circle.cy - dx, circle.cx + dy, circle.cy - dx);

        if (d >= 2 * dx) { d -= 2 * dx + 1; dx++; }
        else if (d < 2 * (circle.r - dy)) { d += 2 * dy - 1; dy--; }
        else { d += 2 * (dy - dx - 1); dy--; dx++; }
    }
}



///////////////////////////////////////////////////////////////////////////
// Triangle Shape
///////////////////////////////////////////////////////////////////////////




void swap_points(int* x, int* y) {
    int t = *x;
    *x = *y;
    *y = t;
}

Triangle triangle_sorted_by_y(Triangle t) {
    if (t.y1 > t.y2) {swap_points(&t.x1, &t.x2);swap_points(&t.y1, &t.y2);}
    if (t.y2 > t.y3) {swap_points(&t.x2, &t.x3);swap_points(&t.y2, &t.y3);}
    if (t.y1 > t.y2) {swap_points(&t.x1, &t.x2);swap_points(&t.y1, &t.y2);}
    return t;
}


void fill_bottom_flat_triangle(SDL_Renderer* renderer, Triangle t){
    const float invslope1 = (float)(t.x2 - t.x1) / (t.y2 - t.y1);
    const float invslope2 = (float)(t.x3 - t.x1) / (t.y3 - t.y1);

    const int y0 = (int) roundf(t.y1);
    const int y1 = (int) roundf(t.y2);

    float curx1 = t.x1;
    float curx2 = t.x1;

    for (int scanline =y0; scanline < y1; scanline++){
        SDL_RenderDrawLine(renderer, (int) roundf(curx1), scanline, (int) roundf(curx2), scanline);
        curx1 += invslope1;
        curx2 += invslope2;
    }
}


void fill_top_flat_triangle(SDL_Renderer* renderer, Triangle t){
    const float invslope1 = (float)(t.x3 - t.x1) / (t.y3 - t.y1);
    const float invslope2 = (float)(t.x3 - t.x2) / (t.y3 - t.y2);

    const int y0 = (int) roundf(t.y3);
    const int y1 = (int) roundf(t.y1);

    float curx1 = t.x3;
    float curx2 = t.x3;

    for (int scanline =y0; scanline > y1; scanline--){
        SDL_RenderDrawLine(renderer, (int) roundf(curx1), scanline, (int) roundf(curx2), scanline);
        curx1 -= invslope1;
        curx2 -= invslope2;
    }
}


void fill_triangle(SDL_Renderer* renderer, Triangle tri){
        tri = triangle_sorted_by_y(tri);

        if (tri.y2 == tri.y3) fill_bottom_flat_triangle(renderer, tri);
        else if (tri.y1 == tri.y2) fill_top_flat_triangle(renderer, tri);
        else {
            int x4 = tri.x1 + ((tri.y2-tri.y1) / (tri.y3-tri.y1) * (tri.x3 - tri.x1));
            int y4 = tri.y2;
            fill_bottom_flat_triangle(renderer, (Triangle) {
                        tri.x1, tri.y1,
                        tri.x2, tri.y2,
                        x4, y4,
                    });
            fill_top_flat_triangle(renderer, (Triangle) {
                        tri.x2, tri.y2,
                        x4, y4,
                        tri.x3, tri.y3,
                    });
            SDL_RenderDrawLine(renderer, tri.x2, tri.y2, x4, y4);
        }

}



///////////////////////////////////////////////////////////////////
// 4 direction 6 components (x, y) for 3 points of a triangle;
float agent_dirs[DIR_CNT][6] = {
    /* DIR_RIGHT, */
    { 0.0, 0.0, 1.0, 0.5, 0.0, 1.0 },
    /* DIR_UP, */
    { 0.0, 1.0, 0.5, 0.0, 1.0, 1.0 },
    /* DIR_LEFT, */
    { 1.0, 0.0, 1.0, 1.0, 0.0, 0.5 },
    /* DIR_DOWN, */
    { 0.0, 0.0, 1.0, 0.0, 0.5, 1.0 },
};

int sdlCheckCode(int code) {
    if (code < 0){
        fprintf(stderr, "SDL error: %s\n", SDL_GetError());
        exit(1);
    }
    return code;
}

void* sdlCheckPointer(void* ptr) {
    if (ptr == NULL){
        fprintf(stderr, "SDL error: %s\n", SDL_GetError());
        exit(1);
    }
    return ptr;
}

void render_board(SDL_Renderer *renderer) {
    sdlCheckCode(SDL_SetRenderDrawColor(renderer, HEX_C(GRID_C)));
    for (int x = 1; x < BOARD_WIDTH; x++){
        sdlCheckCode(SDL_RenderDrawLine(renderer, x*CELL_WIDTH, 0, x*CELL_WIDTH, SCREEN_HEIGHT));
    }
    for (int y = 1; y < BOARD_HEIGHT; y++){
        sdlCheckCode(SDL_RenderDrawLine(renderer, 0, y*CELL_HEIGHT, SCREEN_WIDTH, y*CELL_HEIGHT));
    }
}



void render_agent(SDL_Renderer* renderer, Agent agent) {
    Uint32 color = (agent.health > 0) ? AGENT_ALIVE_C : AGENT_DEAD_C;
    sdlCheckCode(SDL_SetRenderDrawColor(renderer, HEX_C(color)));

    float x1 = agent_dirs[agent.dir][0] * (CELL_WIDTH  - A_PADDING * 2)+ agent.pos.x * CELL_WIDTH  + A_PADDING;
    float y1 = agent_dirs[agent.dir][1] * (CELL_HEIGHT - A_PADDING * 2)+ agent.pos.y * CELL_HEIGHT + A_PADDING;
    float x2 = agent_dirs[agent.dir][2] * (CELL_WIDTH  - A_PADDING * 2)+ agent.pos.x * CELL_WIDTH  + A_PADDING;
    float y2 = agent_dirs[agent.dir][3] * (CELL_HEIGHT - A_PADDING * 2)+ agent.pos.y * CELL_HEIGHT + A_PADDING;
    float x3 = agent_dirs[agent.dir][4] * (CELL_WIDTH  - A_PADDING * 2)+ agent.pos.x * CELL_WIDTH  + A_PADDING;
    float y3 = agent_dirs[agent.dir][5] * (CELL_HEIGHT - A_PADDING * 2)+ agent.pos.y * CELL_HEIGHT + A_PADDING;

    Triangle triangle = { x1, y1, x2, y2, x3, y3 };

    fill_triangle(renderer, triangle);
}

void render_food(SDL_Renderer* renderer, Food food) {
    Circle circle = {
        (int) floorf(food.pos.x * CELL_WIDTH + CELL_WIDTH * 0.5f),
        (int) floorf(food.pos.y * CELL_HEIGHT + CELL_HEIGHT * 0.5f),
        (int) floorf(fminf(CELL_WIDTH, CELL_HEIGHT) * 0.5f - F_PADDING),
    };
    sdlCheckCode(SDL_SetRenderDrawColor(renderer, HEX_C(FOOD_C)));
    fill_circle(renderer, circle);
}



void render_wall(SDL_Renderer* renderer, Wall wall) {
    SDL_Rect rect = {
        (int) floorf(wall.pos.x * CELL_WIDTH),
        (int) floorf(wall.pos.y * CELL_HEIGHT),
        (int) floorf(CELL_WIDTH),
        (int) floorf(CELL_HEIGHT),
    };
    sdlCheckCode(SDL_SetRenderDrawColor(renderer, HEX_C(WALL_C)));
    SDL_RenderFillRect(renderer, &rect);
}



void render_game(SDL_Renderer* renderer, const Game* game) {
    for (size_t i = 0; i < AGENTS_CNT; ++i) {
        //if (game->agents[i].health > 0)
        render_agent(renderer, game->agents[i]);
    }

    for (size_t i = 0; i < FOOD_CNT; ++i) {
        if (!game->foods[i].eaten) render_food(renderer, game->foods[i]);
    }

    for (size_t i = 0; i < WALL_CNT; ++i) {
        render_wall(renderer, game->walls[i]);
    }

}


////////////////////////////////////////////////////////////
