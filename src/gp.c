#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include <SDL2/SDL.h>

#include "game.c"
#include "render.c"


Game game = {0};

int main(int argc, char **argv) {

    // Initialize the seed
    // Everytime the entity position would be different
    srand(time(0));

    //Initialize Game
    init_game(&game);

    // Init SDL Video
    sdlCheckCode(SDL_Init(SDL_INIT_VIDEO));

    // Create SDL window with resizable and shown flags
    SDL_Window *window = sdlCheckPointer(SDL_CreateWindow("UWU", 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_RESIZABLE | SDL_WINDOW_SHOWN));

    // Create an SDL renderer with SDL_RENDERER_ACCELERATED flag
    SDL_Renderer *renderer =  sdlCheckPointer(SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED));

    // Make the screen resolution fixed no matter the window resolution
    sdlCheckCode( SDL_RenderSetLogicalSize(renderer, SCREEN_WIDTH, SCREEN_HEIGHT) );


    // Game loop
    int quit = 0;
    while(!quit) {
        SDL_Event event;
        while(SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT: { quit = 1; }
                    break;
                case SDL_KEYDOWN: {
                    switch (event.key.keysym.sym) {
                        case SDLK_SPACE: {
                            step_game(&game);
                        } break;
                        case SDLK_r: {
                            init_game(&game);
                        } break;
                        case SDLK_q: {
                            quit = 1;
                        } break;
                    }
                } break;
                case SDL_MOUSEBUTTONDOWN: {
                    Coord pos;
                    pos.x = (int) floorf(event.button.x / CELL_WIDTH);
                    pos.y = (int) floorf(event.button.y / CELL_WIDTH);
                    Agent* agent = agent_at(&game, pos);
                    if (agent) print_agent(stdout, agent);
                } break;
            }
        }

        // Sets the renderer with a particular color
        sdlCheckCode(SDL_SetRenderDrawColor(renderer, HEX_C(BG_C)));

        // Clears the renderer
        sdlCheckCode(SDL_RenderClear(renderer));

        // Renders the board/ grid.
        render_board(renderer);

        // Renders the game components.
        render_game(renderer, &game);

        // Presents the rendered things
        SDL_RenderPresent(renderer);
    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
    return 0;
}
