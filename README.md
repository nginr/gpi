# Visualizing Genetic Programmming

## Quick Start

Dependencies: [SDL2]

```console
$ make run
```

## Controls

#### Keyboard

| Key              | Action                                                    |
|------------------|-----------------------------------------------------------|
| <kbd>q</kbd>     | Quit the game                                             |
| <kbd>r</kbd>     | Generate completely new state of the game                 |
| <kbd>SPACE</kbd> | Step the state of the game                                |

[SDL2](https://www.libsdl2.org/)
